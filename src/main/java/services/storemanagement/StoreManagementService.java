package services.storemanagement;

import com.tradr.springboot.view.storeclasses.*;
import com.tradr.springboot.view.userclasses.UserAuthKey;
import java.io.ByteArrayInputStream;
import java.sql.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import org.springframework.stereotype.Service;
import services.interfaces.CheckProfanity;
import services.registration.UserManagementService;
import services.resourceprocessor.ProfanityProcessorService;
import services.utils.DatabaseVerification;
import services.utils.HashUtils;
import services.utils.StoreEnums;

@Service
public class StoreManagementService {

	public StoreEnums insertStore(
		Store store,
		UserManagementService userManagementService
	) {
		CheckProfanity check = (String stringToCheck) ->
			ProfanityProcessorService.inspectString(
				stringToCheck.toLowerCase(Locale.getDefault())
			);

		if (
			check.call(store.getStoreTitle()) ||
			check.call(store.getStoreDescription()) ||
			check.call(store.getPostcode()) ||
			check.call(store.getAddressLine1()) ||
			check.call(store.getAddressLine2()) ||
			check.call(store.getAddressLine3())
		) {
			return StoreEnums.STORE_CREATION_FAILED_PROFANITY;
		}

		// Get the associated authkey and assign it to an authkey object.
		// @TODO Do we need this to be an actual object? Probably. It may need to be expanded upon later.
		UserAuthKey authKey = new UserAuthKey();
		authKey.setAuthKey(store.getAuthKey());

		// Check to see whether the authkey is in date and return an invalid user if it isn't.
		boolean isUserValid = userManagementService.isAuthKeyValid(authKey);

		if (!isUserValid) {
			return StoreEnums.INVALID_USER;
		}
		// Check to see if the user actually has an extant store, users may only have one store (As of 09/06/2023 this could be expanded to include more.)
		String userHasStore = userManagementService.getUserOwnedStoreUUID(
			authKey.getAuthKey()
		);

		if (!Objects.equals(userHasStore, "")) {
			return StoreEnums.STORE_CREATION_FAILED_STORE_EXISTS;
		}

		// Validate the UUID? This seems kind of redundant right now because we're doing an auth call above but this won't hurt to keep?
		// This was probably how we intended to initially build the validation for the user's ability to add a store. UUIDs are now a 'soft secret'?
		// @TODO Revisit whether we need this.
		CompletableFuture<Boolean> validateUUID = CompletableFuture.supplyAsync(() -> {
				try {
					return isUUIDValid(store.getParentUUID());
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		);

		if (!validateUUID.join()) {
			return StoreEnums.INVALID_UUID;
		}

		// Insert the store into the stores table.
		CompletableFuture<StoreEnums> insertStoreCompletableFuture = CompletableFuture.supplyAsync(() -> {
				String craftTagsAsString = String.join(
					",",
					store.getCraftTags()
				);
				String storeOwnUUD = HashUtils.generateUUID();
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"INSERT INTO stores (" +
						"storeTitle, " +
						"storeDescription, " +
						"canMessage, " +
						"isPrivate, " +
						"storeTheme, " +
						"craftTags, " +
						"addressLine1, " +
						"addressLine2, " +
						"addressLine3, " +
						"postcode, " +
						"parentUUID, " +
						"ownUUID, " +
						"publicStoreId, " +
						"isSuspended, " +
						"isSuspendedDuration, " +
						"isSuspendedReason, " +
						"suspensionCount, " +
						"reportedByUsers, " +
						"isDisabled, " +
						"isDisabledReason, " +
						"storePhone, " +
						"storeAvatar, " +
						"storeBanner) VALUES " +
						"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
					);
				) {
					ByteArrayInputStream inputStream = new ByteArrayInputStream(
						Base64.getDecoder().decode(store.getStoreBanner())
					);
					statement.setString(1, store.getStoreTitle());
					statement.setString(2, store.getStoreDescription());
					statement.setBoolean(3, true);
					statement.setBoolean(4, store.isPrivate());
					statement.setString(5, store.getStoreTheme());
					statement.setString(6, craftTagsAsString);
					statement.setString(7, store.getAddressLine1());
					statement.setString(8, store.getAddressLine2());
					statement.setString(9, store.getAddressLine3());
					statement.setString(10, store.getPostcode());
					statement.setString(11, store.getParentUUID());
					statement.setString(12, storeOwnUUD);

					statement.setString(
						13,
						HashUtils.generatePrimaryPublicId(store.getStoreTitle())
					);

					statement.setBoolean(14, false);
					statement.setString(15, "");
					statement.setString(16, "");
					statement.setInt(17, 0);
					statement.setString(18, "");
					statement.setBoolean(19, false);
					statement.setString(20, "");
					statement.setString(21, "07990 1800180");
					if (store.getStoreAvatar() != null) {
						statement.setBinaryStream(
							22,
							inputStream,
							inputStream.available()
						);
					} else {
						statement.setNull(22, Types.BLOB);
					}

					if (store.getStoreBanner() != null) {
						statement.setBinaryStream(
							23,
							inputStream,
							inputStream.available()
						);
					} else {
						statement.setNull(23, Types.BLOB);
					}

					int rowsInserted = statement.executeUpdate();

					// Update the user's owned store UUID, over-writing the existing one.
					boolean userOwnedStoresUUIDUpdated = userManagementService.updateUserOwnedStoreUUID(
						storeOwnUUD,
						store.getParentUUID(),
						false
					);

					return (rowsInserted > 0 && userOwnedStoresUUIDUpdated)
						? StoreEnums.STORE_INSERTED
						: StoreEnums.STORE_INSERTION_FAILED;
				} catch (SQLException e) {
					return StoreEnums.STORE_INSERTION_FAILED;
				}
			}
		);

		return insertStoreCompletableFuture.join();
	}

	public StoreEnums prepareStoreItemsForInsertion(
		StoreItem storeItem,
		String parentUUID
	) {
		CheckProfanity check = (String stringToCheck) ->
			ProfanityProcessorService.inspectString(
				stringToCheck.toLowerCase(Locale.getDefault())
			);

		if (
			check.call(storeItem.getStoreItemDescription()) ||
			check.call(storeItem.getStoreItemName())
		) {
			return StoreEnums.ITEM_INSERTION_FAILED_PROFANITY;
		}
		// Place a single store item into the storeItems table.
		CompletableFuture<StoreEnums> prepareStoreItemsForInsertionCompletableFuture = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"INSERT INTO storeitems (storeItemName, parentUUID, storeItemImage, storeItemDescription, storeItemPrice, storeItemPublicId) VALUES " +
						"(?, ?, ?, ?, ?, ?)"
					);
				) {
					ByteArrayInputStream inputStream = new ByteArrayInputStream(
						Base64
							.getDecoder()
							.decode(storeItem.getStoreItemImage())
					);
					statement.setString(1, storeItem.getStoreItemName());
					statement.setString(2, parentUUID);
					if (storeItem.getStoreItemImage() != null) {
						statement.setBinaryStream(
							3,
							inputStream,
							inputStream.available()
						);
					} else {
						statement.setNull(3, Types.BLOB);
					}
					statement.setString(4, storeItem.getStoreItemDescription());
					statement.setString(5, storeItem.getStoreItemPrice());
					statement.setString(
						6,
						HashUtils.generatePublicId(storeItem.getStoreItemName())
					);
					int rowsInserted = statement.executeUpdate();
					return (rowsInserted > 0)
						? StoreEnums.ITEM_INSERTED
						: StoreEnums.ITEM_INSERTION_FAILED;
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);

		return prepareStoreItemsForInsertionCompletableFuture.join();
	}

	public StoreEnums updateStore(
		StoreUpdate storeUpdate,
		UserManagementService userManagementService
	) {
		UserAuthKey authKey = new UserAuthKey();
		authKey.setAuthKey(storeUpdate.getAuthKey());

		if (!userManagementService.isAuthKeyValid(authKey)) {
			return StoreEnums.STORE_UPDATE_FAILED;
		}

		String ownedStoreUUID = userManagementService.getUserOwnedStoreUUID(
			storeUpdate.getAuthKey()
		);

		CompletableFuture<StoreEnums> updateStoreCompletableFuture = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"UPDATE stores SET storeDescription = IF(?, ?, storeDescription), " +
						"storeTheme = IF(?, ?, storeTheme),  " +
						"isPrivate = ?,  " +
						"addressLine1 = IF(?, ?, addressLine1),  " +
						"addressLine2 = IF(?, ?, addressLine2),  " +
						"addressLine3 = IF(?, ?, addressLine3),  " +
						"storePhone = IF(?, ?, storePhone),  " +
						"postcode = IF(?, ?, postcode),  " +
						"storeAvatar = IF(?, ?, storeAvatar),  " +
						"storeBanner = IF(?, ?, storeBanner)  " +
						"WHERE ownUUID = ?"
					)
				) {
					statement.setBoolean(
						1,
						!storeUpdate.getStoreDescription().isEmpty()
					);
					statement.setString(2, storeUpdate.getStoreDescription());

					statement.setBoolean(
						3,
						!storeUpdate.getStoreTheme().isEmpty()
					);
					statement.setString(4, storeUpdate.getStoreTheme());

					statement.setBoolean(5, storeUpdate.isPrivate());

					statement.setBoolean(
						6,
						!storeUpdate.getAddressLine1().isEmpty()
					);
					statement.setString(7, storeUpdate.getAddressLine1());

					statement.setBoolean(
						8,
						!storeUpdate.getAddressLine2().isEmpty()
					);
					statement.setString(9, storeUpdate.getAddressLine2());

					statement.setBoolean(
						10,
						!storeUpdate.getAddressLine3().isEmpty()
					);
					statement.setString(11, storeUpdate.getAddressLine3());

					statement.setBoolean(
						12,
						!storeUpdate.getStorePhone().isEmpty()
					);
					statement.setString(13, storeUpdate.getStorePhone());

					statement.setBoolean(
						14,
						!storeUpdate.getPostcode().isEmpty()
					);
					statement.setString(15, storeUpdate.getPostcode());

					if (!storeUpdate.getStoreAvatar().isEmpty()) {
						ByteArrayInputStream inputStream = new ByteArrayInputStream(
							Base64
								.getDecoder()
								.decode(storeUpdate.getStoreAvatar())
						);

						statement.setBoolean(
							16,
							!storeUpdate.getStoreAvatar().isEmpty()
						);
						statement.setBinaryStream(
							17,
							inputStream,
							inputStream.available()
						);
					} else {
						statement.setBoolean(
							16,
							!storeUpdate.getStoreAvatar().isEmpty()
						);
						statement.setNull(17, Types.BLOB);
					}

					if (!storeUpdate.getStoreBanner().isEmpty()) {
						ByteArrayInputStream inputStream = new ByteArrayInputStream(
							Base64
								.getDecoder()
								.decode(storeUpdate.getStoreBanner())
						);

						statement.setBoolean(
							18,
							!storeUpdate.getStoreBanner().isEmpty()
						);
						statement.setBinaryStream(
							19,
							inputStream,
							inputStream.available()
						);
					} else {
						statement.setBoolean(
							18,
							!storeUpdate.getStoreBanner().isEmpty()
						);
						statement.setNull(19, Types.BLOB);
					}

					statement.setString(20, ownedStoreUUID);

					int rowsUpdated = statement.executeUpdate();
					return rowsUpdated > 0
						? StoreEnums.STORE_UPDATED
						: StoreEnums.STORE_UPDATE_FAILED;
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);

		return updateStoreCompletableFuture.join();
	}

	public StoreSummaryResponse getStoresListSummaryFromDatabase(
		String customSearchStatement,
		UserManagementService userManagementService,
		boolean isAdminQuery,
		String limit
	) {
		// Get a summarised list of stores from the database.
		// @TODO are we sure we want to send back the actual storeUUID in this response?

		CompletableFuture<StoreSummaryResponse> storeSummaryResponseCompletableFuture = CompletableFuture.supplyAsync(() -> {
				StoreSummaryResponse response = new StoreSummaryResponse();
				response.setStoreSummaryQueryStatus(
					StoreEnums.STORE_LIST_EMPTY
				);
				List<StoreSummary> storeSummaries = new ArrayList<StoreSummary>();

				String sqlStatement =
					"SELECT storeTitle, storeDescription, storeTheme, ownUUID, storeBanner, publicStoreId, suspensionCount, parentUUID FROM stores WHERE isPrivate = FALSE AND isSuspended = FALSE AND isDisabled = FALSE LIMIT " +
					limit;

				if (customSearchStatement != "") {
					sqlStatement = customSearchStatement;
				}

				if (isAdminQuery) {
					sqlStatement =
						"SELECT storeTitle, storeDescription, storeTheme, ownUUID, storeBanner, publicStoreId, suspensionCount, parentUUID, reportedByUsers FROM stores WHERE reportedByUsers <> ''";
				}

				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						sqlStatement
					);
					ResultSet rs = statement.executeQuery();
				) {
					int numberOfReports = 0;

					if (!rs.next()) {
						return response;
					}

					do {
						response.setStoreSummaryQueryStatus(
							StoreEnums.STORE_LIST_POPULATED
						);
						StoreSummary storeSummary = new StoreSummary();

						if (isAdminQuery) {
							numberOfReports =
								rs
									.getString("reportedByUsers")
									.split(",")
									.length;
							storeSummary.setOwnUUID(rs.getString("ownUUID"));
						}

						storeSummary.setStoreTitle(rs.getString("storeTitle"));
						storeSummary.setStoreDescription(
							rs.getString("storeDescription")
						);

						storeSummary.setStoreTheme(rs.getString("storeTheme"));
						storeSummary.setPublicStoreId(
							rs.getString("publicStoreId")
						);
						storeSummary.setSuspensionCount(
							rs.getInt("suspensionCount")
						);
						storeSummary.setAssociatedUser(
							userManagementService.getAssociatedUser(
								rs.getString("parentUUID")
							)
						);
						storeSummary.setReports(numberOfReports);

						storeSummaries.add(storeSummary);
					} while (rs.next());

					response.setStores(storeSummaries);
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}

				return response;
			}
		);

		return storeSummaryResponseCompletableFuture.join();
	}

	public StoreResponse getIndividualStore(
		String storeId,
		boolean withReportedByUsers,
		UserManagementService userManagementService
	) {
		// Get the individual store's details using the public store ID, i.e. lucys-bakes-95739271
		// @TODO This response sends back the Store object as a part of StoreResponse, this isn't a safe object to send back and it should be refactored.
		// ^ FIXED.
		CompletableFuture<StoreResponse> getIndividualStoreResponseCompletableFuture = CompletableFuture.supplyAsync(() -> {
				StoreResponse response = new StoreResponse();
				response.setStoreQueryResponseStatus(StoreEnums.STORE_EMPTY);

				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"SELECT * FROM stores WHERE publicStoreId = ? OR parentUUID = ? ORDER BY timestamp DESC"
					);
				) {
					statement.setString(1, storeId);
					statement.setString(2, storeId);
					ResultSet rs = statement.executeQuery();

					if (!rs.next()) {
						return response;
					}

					PublicStoreDetails store = new PublicStoreDetails();

					response.setStoreQueryResponseStatus(
						StoreEnums.STORE_POPULATED
					);

					if (withReportedByUsers) {
						String[] reportedByUsers = rs
							.getString("reportedByUsers")
							.split(",");
						List<String> reportedByUsersArray = new ArrayList<>(
							Arrays.asList(reportedByUsers)
						);
						store.setReportedByUsers(reportedByUsersArray);
					}

					String[] craftingTags = rs
						.getString("craftTags")
						.split(",");

					List<String> craftingTagsAsList = new ArrayList<>(
						Arrays.asList(craftingTags)
					);
					List<StoreItem> storeItems = getStoreItemsForIndividualStore(
						rs.getString("ownUUID")
					);

					store.setStoreTitle(rs.getString("storeTitle"));
					store.setStoreDescription(rs.getString("storeDescription"));
					store.setCanMessage(rs.getBoolean("canMessage"));
					store.setPrivate(rs.getBoolean("isPrivate"));
					store.setStoreTheme(rs.getString("storeTheme"));
					store.setCraftTags(craftingTagsAsList);
					store.setAddressLine1(rs.getString("addressLine1"));
					store.setAddressLine2(rs.getString("addressLine2"));
					store.setAddressLine3(rs.getString("addressLine3"));
					store.setPostcode(rs.getString("postcode"));
					store.setStorePhone(rs.getString("storePhone"));
					store.setPublicStoreId(rs.getString("publicStoreID"));
					store.setSuspended(rs.getBoolean("isSuspended"));
					store.setIsSuspendedDuration(
						rs.getString("isSuspendedDuration")
					);
					store.setIsSuspendedReason(
						rs.getString("isSuspendedReason")
					);
					store.setSuspensionCount(rs.getInt("suspensionCount"));
					store.setDisabled(rs.getBoolean("isDisabled"));
					store.setIsDisabledReason(rs.getString("isDisabledReason"));

					store.setStoreItems(storeItems);

					store.setAssociatedUser(
						userManagementService.getAssociatedUser(
							rs.getString("parentUUID")
						)
					);

					response.setStore(store);
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}

				return response;
			}
		);

		return getIndividualStoreResponseCompletableFuture.join();
	}

	private List<StoreItem> getStoreItemsForIndividualStore(
		String storeItemsParentUUID
	) throws SQLException {
		List<StoreItem> storeItemsFinal = new ArrayList<StoreItem>();

		// Using the store's actual UUID, get all items from the storeItems table which match the UUID.
		CompletableFuture<List<StoreItem>> getStoreItemsForIndividualStoreCompletableFuture = CompletableFuture.supplyAsync(() -> {
				List<StoreItem> storeItems = new ArrayList<StoreItem>();
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"SELECT * FROM storeItems WHERE parentUUID = ?"
					);
				) {
					statement.setString(1, storeItemsParentUUID);
					ResultSet rs = statement.executeQuery();

					if (!rs.next()) {
						return storeItems;
					}

					do {
						StoreItem storeItem = new StoreItem();
						storeItem.setStoreItemName(
							rs.getString("storeItemName")
						);
						storeItem.setStoreItemDescription(
							rs.getString("storeItemDescription")
						);
						storeItem.setStoreItemPrice(
							rs.getString("storeItemPrice")
						);
						storeItem.setStoreItemPublicId(
							rs.getString("storeItemPublicId")
						);
						storeItems.add(storeItem);
					} while (rs.next());
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
				return storeItems;
			}
		);

		storeItemsFinal =
			getStoreItemsForIndividualStoreCompletableFuture.join();

		return storeItemsFinal;
	}

	public StoreEnums deleteItem(
		String storeItemPublicId,
		String authKey,
		UserManagementService userManagementService
	) {
		// Get the user's owned store UUID by auth key.
		String userChildStoreUUID = userManagementService.getUserOwnedStoreUUID(
			authKey
		);
		// If the user has no child store then they can't delete items from a store.
		if (Objects.equals(userChildStoreUUID, "")) {
			return StoreEnums.ITEM_DELETION_FAILED;
		}
		// Delete items where the public store item ID (i.e. @lucys-bakes-9573) and the 'parent UUID' which is the user's 'child store UUID' (which is basically the store's UUID)
		CompletableFuture<StoreEnums> deleteItemCompletableFuture = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"DELETE FROM storeItems WHERE storeItemPublicId = ? AND parentUUID = ?;"
					);
				) {
					statement.setString(1, storeItemPublicId);
					statement.setString(2, userChildStoreUUID);
					int rowsDeleted = statement.executeUpdate();
					return (rowsDeleted > 0)
						? StoreEnums.ITEM_DELETED
						: StoreEnums.ITEM_DELETION_FAILED;
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);
		return deleteItemCompletableFuture.join();
	}

	public StoreEnums deleteStore(
		UserAuthKey authKey,
		String userChildStoreUUID
	) {
		// Delete any relevant items that are a part of the user's owned store.
		CompletableFuture<StoreEnums> deleteStoreItemsFromStore = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"DELETE FROM storeItems WHERE parentUUID = ?"
					);
				) {
					statement.setString(1, userChildStoreUUID);

					int itemsDeleted = statement.executeUpdate();

					return (itemsDeleted > 0)
						? StoreEnums.ITEMS_DELETED
						: StoreEnums.ITEMS_DELETION_FAILED;
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);
		// Might be used later?
		deleteStoreItemsFromStore.join();
		// Delete the store owned by the user.
		CompletableFuture<StoreEnums> deleteIndividualStoreCompletableFuture = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"DELETE FROM stores WHERE ownUUID = ?"
					);
				) {
					statement.setString(1, userChildStoreUUID);
					int rowsDeleted = statement.executeUpdate();
					return (rowsDeleted > 0)
						? StoreEnums.STORE_DELETION_SUCCESSFUL
						: StoreEnums.STORE_DELETION_FAILED;
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);
		return deleteIndividualStoreCompletableFuture.join();
	}

	public boolean isUUIDValid(String uuid) throws Exception {
		try (
			Connection conn = DatabaseVerification.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"SELECT * FROM users WHERE uuid = '" + uuid + "'"
			);
			ResultSet rs = statement.executeQuery()
		) {
			if (!rs.next()) {
				throw new Exception("No such UUID key");
			}
			return true;
		} catch (SQLException e) {
			throw new SQLException(e);
		}
	}

	public String getUserUUID(String authKey) {
		String userUUID = "";
		try (
			Connection conn = DatabaseVerification.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"SELECT * FROM users WHERE authKey = '" + authKey + "'"
			);
			ResultSet rs = statement.executeQuery()
		) {
			if (!rs.next()) {
				return userUUID;
			}

			do {
				userUUID = rs.getString("uuid");
			} while (rs.next());

			return userUUID;
		} catch (SQLException e) {
			return userUUID;
		}
	}
}
