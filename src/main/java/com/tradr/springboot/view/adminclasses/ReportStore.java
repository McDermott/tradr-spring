package com.tradr.springboot.view.adminclasses;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ReportStore {

	private String authKey;
	private String reportingUUID;
	private String storePublicId;
}
