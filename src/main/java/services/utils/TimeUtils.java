package services.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TimeUtils {

	public static String getDateNowAsISODateTimeString() {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		return formatter.format(LocalDate.now().atStartOfDay());
	}

	public static String getDateNowPlus6MonthsAsISODateTimeString() {
		LocalDateTime plus6Months = LocalDate
			.now()
			.plusMonths(6)
			.atStartOfDay();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		return formatter.format(plus6Months);
	}

	public static boolean isISOStringOutOfDate(String isoDateString) {
		LocalDate isoDate = LocalDate.parse(
			isoDateString,
			DateTimeFormatter.ISO_DATE_TIME
		);
		LocalDate now = LocalDate.parse(
			getDateNowAsISODateTimeString(),
			DateTimeFormatter.ISO_DATE_TIME
		);
		return isoDate.isBefore(now);
	}

	public static String getSuspensionDuration(int suspensionCount) {
		LocalDateTime suspensionTime;
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		switch (suspensionCount) {
			case 1:
				suspensionTime = LocalDate.now().plusDays(3).atStartOfDay();
				break;
			case 2:
				suspensionTime = LocalDate.now().plusDays(7).atStartOfDay();
				break;
			case 3:
				suspensionTime = LocalDate.now().plusDays(14).atStartOfDay();
				break;
			case 4:
				suspensionTime = LocalDate.now().plusDays(30).atStartOfDay();
				break;
			case 5:
				suspensionTime = LocalDate.now().plusDays(90).atStartOfDay();
				break;
			default:
				suspensionTime = LocalDate.now().plusDays(365).atStartOfDay();
		}
		return formatter.format(suspensionTime);
	}
}
