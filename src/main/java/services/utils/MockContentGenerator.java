package services.utils;

import java.io.ByteArrayInputStream;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.*;

public class MockContentGenerator {

	private static final Random random = new Random();
	private static final DecimalFormat decimalFormat = new DecimalFormat(
		"£0.00"
	);

	public static String generateRandomPrice() {
		double price = random.nextDouble() * 999 + 1; // Generate a random number between 1 and 1000
		return decimalFormat.format(price);
	}

	public static String getRandom(String[] array) {
		Random random = new Random();
		int randomIndex = random.nextInt(array.length);
		return array[randomIndex];
	}

	public static String getRandomList(String[] items) {
		Random random = new Random();
		int itemCount = random.nextInt(3) + 1; // Random number of items between 1 and 3
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < itemCount; i++) {
			int randomIndex = random.nextInt(items.length);
			sb.append(items[randomIndex]);

			if (i < itemCount - 1) {
				sb.append(", ");
			}
		}

		return sb.toString();
	}

	public static void createUsersAndStores()
		throws SQLException, NoSuchAlgorithmException {
		for (int i = 0; i < 100; i++) {
			// STORE DETAILS
			String storeNoun = MockContentGenerator.getRandom(
				MockStaticData.NOUNS
			);
			String storePlace = MockContentGenerator.getRandom(
				MockStaticData.PLACES
			);

			String storeName = String.format("%s %s", storeNoun, storePlace);
			String storeDescription = String.format(
				MockContentGenerator.getRandom(MockStaticData.TAG_LINES),
				storeNoun
			);
			String storePublicId = HashUtils.generatePrimaryPublicId(storeName);
			String address1 = MockContentGenerator.getRandom(
				MockStaticData.ADDRESS_1
			);
			String address2 = MockContentGenerator.getRandom(
				MockStaticData.ADDRESS_2
			);
			String postCode = MockContentGenerator.getRandom(
				MockStaticData.POST_CODES
			);
			String storeTheme = MockContentGenerator.getRandom(
				MockStaticData.STORE_THEMES
			);
			String storeTagList = MockContentGenerator.getRandomList(
				MockStaticData.CRAFT_TAGS
			);
			String storeUUID = HashUtils.generateUUID();

			// USER DETAILS
			String userFirstName = MockContentGenerator.getRandom(
				MockStaticData.FIRST_NAMES
			);
			String userLastName = MockContentGenerator.getRandom(
				MockStaticData.LAST_NAMES
			);
			String userName = HashUtils.generatePrimaryPublicId(
				userFirstName + "-" + userLastName
			);
			String userEmail = String.format(
				"%s-%s@%s-%s.com",
				userFirstName.toLowerCase(),
				userLastName.toLowerCase(),
				storeNoun.toLowerCase(),
				storePlace.toLowerCase()
			);
			String userUUID = HashUtils.generateUUID();

			Connection conn = DatabaseVerification.getConnection();

			// INSERT USER
			PreparedStatement statement_user = conn.prepareStatement(
				"INSERT INTO users (userName, firstName, lastName, email, hash, salt, authKey, authKeyExpiry, dob, avatar, uuid, ownedStoreUUID, registrationDate, isSuspended, isSuspendedDuration, isSuspendedReason, suspensionCount, isDisabled, isDisabledReason, isAdmin) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
			);

			String salt = HashUtils.generateSalt();
			String hash = HashUtils.hashWithSalt("test", salt);
			String authKey = HashUtils.hashWithSalt(userEmail, salt);
			ByteArrayInputStream userAvatar = new ByteArrayInputStream(
				Base64.getDecoder().decode(MockImages.USER_AVATAR_IMAGE)
			);

			statement_user.setString(1, userName);
			statement_user.setString(2, userFirstName);
			statement_user.setString(3, userLastName);
			statement_user.setString(4, userEmail);
			statement_user.setString(5, hash);
			statement_user.setString(6, salt);
			statement_user.setString(7, authKey);
			statement_user.setString(
				8,
				TimeUtils.getDateNowPlus6MonthsAsISODateTimeString()
			);
			statement_user.setString(9, "01/01/1990");
			statement_user.setBinaryStream(
				10,
				userAvatar,
				userAvatar.available()
			);
			statement_user.setString(11, userUUID);
			statement_user.setString(12, storeUUID);
			statement_user.setString(
				13,
				TimeUtils.getDateNowAsISODateTimeString()
			);

			statement_user.setBoolean(14, false);
			statement_user.setString(15, "");
			statement_user.setString(16, "");
			statement_user.setInt(17, 0);
			statement_user.setBoolean(18, false);
			statement_user.setString(19, "");
			statement_user.setBoolean(20, false);

			statement_user.executeUpdate();

			// INSERT STORE
			PreparedStatement statement_store = conn.prepareStatement(
				"INSERT INTO stores (" +
				"storeTitle, " +
				"storeDescription, " +
				"canMessage, " +
				"isPrivate, " +
				"storeTheme, " +
				"craftTags, " +
				"addressLine1, " +
				"addressLine2, " +
				"addressLine3, " +
				"postcode, " +
				"parentUUID, " +
				"ownUUID, " +
				"publicStoreId, " +
				"isSuspended, " +
				"isSuspendedDuration, " +
				"isSuspendedReason, " +
				"suspensionCount, " +
				"reportedByUsers, " +
				"isDisabled, " +
				"isDisabledReason, " +
				"storePhone, " +
				"storeAvatar, " +
				"storeBanner) VALUES " +
				"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
			);

			ByteArrayInputStream storeAvatar = new ByteArrayInputStream(
				Base64.getDecoder().decode(MockImages.STORE_AVATAR_IMAGE)
			);

			ByteArrayInputStream storeBanner = new ByteArrayInputStream(
				Base64.getDecoder().decode(MockImages.STORE_BANNER_IMAGE)
			);

			statement_store.setString(1, storeName);
			statement_store.setString(2, storeDescription);
			statement_store.setBoolean(3, true);
			statement_store.setBoolean(4, false);
			statement_store.setString(5, storeTheme);
			statement_store.setString(6, storeTagList);
			statement_store.setString(7, address1);
			statement_store.setString(8, address2);
			statement_store.setString(9, "Co. Antrim");
			statement_store.setString(10, postCode);
			statement_store.setString(11, userUUID);
			statement_store.setString(12, storeUUID);
			statement_store.setString(13, storePublicId);
			statement_store.setBoolean(14, false);
			statement_store.setString(15, "");
			statement_store.setString(16, "");
			statement_store.setInt(17, 0);
			statement_store.setString(18, "");
			statement_store.setBoolean(19, false);
			statement_store.setString(20, "");
			statement_store.setString(21, "07990 1800180");
			statement_store.setBinaryStream(
				22,
				storeAvatar,
				storeAvatar.available()
			);
			statement_store.setBinaryStream(
				23,
				storeBanner,
				storeBanner.available()
			);

			statement_store.executeUpdate();

			// INSERT MOCK ITEMS
			Random random = new Random();
			int iterations = random.nextInt(20) + 1; // Generates a random number between 1 and 20

			for (int j = 0; j < iterations; j++) {
				String itemName = String.format(
					MockContentGenerator.getRandom(
						MockStaticData.ITEM_PREFIXES
					),
					storeNoun
				);

				PreparedStatement statement_items = conn.prepareStatement(
					"INSERT INTO storeitems (storeItemName, parentUUID, storeItemImage, storeItemDescription, storeItemPrice, storeItemPublicId) VALUES " +
					"(?, ?, ?, ?, ?, ?)"
				);

				ByteArrayInputStream storeItemImage = new ByteArrayInputStream(
					Base64.getDecoder().decode(MockImages.STORE_PRODUCT_IMAGE)
				);
				statement_items.setString(1, itemName);
				statement_items.setString(2, storeUUID);

				statement_items.setBinaryStream(
					3,
					storeItemImage,
					storeItemImage.available()
				);

				statement_items.setString(
					4,
					String.format(
						MockContentGenerator.getRandom(
							MockStaticData.ITEM_SUFFIXES
						),
						itemName
					)
				);
				statement_items.setString(5, generateRandomPrice());
				statement_items.setString(
					6,
					HashUtils.generatePublicId(itemName)
				);

				statement_items.executeUpdate();
			}
		}
	}
}
