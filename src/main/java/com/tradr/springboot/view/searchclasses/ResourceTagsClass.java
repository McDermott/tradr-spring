package com.tradr.springboot.view.searchclasses;

import java.util.HashMap;
import java.util.Map;

public class ResourceTagsClass {

	public static Map<String, String> getCraftTags() {
		Map<String, String> craftTags = new HashMap<>();
		craftTags.put("metal_working", "Metal Working");
		craftTags.put("wood_working", "Wood Working");
		craftTags.put("paper_working", "Paper Working");
		craftTags.put("jewelry", "Jewelry");
		craftTags.put("stone_working", "Stone Working");
		craftTags.put("foodstuffs", "Foodstuffs");
		craftTags.put("knitting", "Knitting");
		craftTags.put("needle_working", "Needle Working");
		craftTags.put("sewing", "Sewing");
		craftTags.put("tailoring", "Tailoring");
		craftTags.put("painting", "Painting");
		craftTags.put("drawing", "Drawing");
		craftTags.put("sculpting", "Sculpting");
		craftTags.put("clay_working", "Clay Working");
		craftTags.put("floral_working", "Floral Working");
		craftTags.put("gardening", "Gardening");
		craftTags.put("light_working", "Light Working");
		craftTags.put("leather_working", "Leather Working");
		craftTags.put("herbal_working", "Herbal Working");
		craftTags.put("traditional_supplements", "Traditional Supplements");
		craftTags.put("joinery", "Joinery");
		craftTags.put("inlay", "Inlay");
		craftTags.put("marketry", "Marketry");
		craftTags.put("pottery", "Pottery");
		craftTags.put("weaving", "Weaving");
		craftTags.put("crochet", "Crochet");
		craftTags.put("embroidery", "Embroidery");
		craftTags.put("wood_carving", "Wood Carving");
		craftTags.put("calligraphy", "Calligraphy");
		craftTags.put("bookbinding", "Bookbinding");
		craftTags.put("quilting", "Quilting");
		craftTags.put("origami", "Origami");
		craftTags.put("doll_making", "Doll Making");
		craftTags.put("bead_work", "Bead Work");
		craftTags.put("macrame", "Macrame");
		craftTags.put("candle_making", "Candle Making");
		craftTags.put("felting", "Felting");
		craftTags.put("tabletop", "Tabletop");
		craftTags.put("religious", "Religious");
		craftTags.put("seasonal", "Seasonal");
		craftTags.put("garden_decore_building", "Garden Decore Building");
		craftTags.put("carpentry", "Carpentry");
		craftTags.put("electrical_work", "Electrical Work");
		craftTags.put("masonry", "Masonry");
		craftTags.put("welding", "Welding");
		craftTags.put("plumbing", "Plumbing");
		craftTags.put("roofing", "Roofing");
		craftTags.put("car_repair", "Car Repair");
		craftTags.put("car_painting", "Car Painting");
		craftTags.put("fence_installation", "Fence Installation");
		craftTags.put("drywall_installation", "Drywall Installation");
		craftTags.put("house_painting", "House Painting");
		craftTags.put("window_cleaning", "Window Cleaning");
		craftTags.put("home_staging", "Home Staging");
		craftTags.put("housekeeping_services", "Housekeeping Services");
		craftTags.put("upholstery", "Upholstery");
		craftTags.put("blacksmithing", "Blacksmithing");
		craftTags.put("glassblowing", "Glassblowing");
		craftTags.put("art_restoration", "Art Restoration");
		craftTags.put("tattoo_artist", "Tattoo Artist");
		craftTags.put("makeup_artistry", "Makeup Artistry");
		craftTags.put("henna_tattooing", "Henna Tattooing");
		craftTags.put("computer_repair", "Computer Repair");
		craftTags.put("auto_detailing", "Auto Detailing");
		craftTags.put("locksmithing", "Locksmithing");
		craftTags.put("event_decoration", "Event Decoration");
		craftTags.put("archery_lessons", "Archery Lessons");
		craftTags.put("fishing_guide", "Fishing Guide");
		craftTags.put("tax_preparation", "Tax Preparation");
		craftTags.put("financial_consulting", "Financial Consulting");
		craftTags.put("personal_shopping", "Personal Shopping");
		craftTags.put("home_organizing", "Home Organizing");
		craftTags.put("outdoor_aquatic_servicing", "Outdoor Aquatic Servicing");
		craftTags.put("tree_surgery", "Tree Surgery");
		craftTags.put("gourmet_catering", "Gourmet Catering");
		craftTags.put("personal_chef_services", "Personal Chef Services");
		craftTags.put("piano_tuning", "Piano Tuning");
		craftTags.put("voice_coaching", "Voice Coaching");
		craftTags.put("professional_cleaning", "Professional Cleaning");
		return craftTags;
	}

	public static Map<String, String> getThemeTags() {
		Map<String, String> themeTags = new HashMap<String, String>();
		themeTags.put("default", "default"); // White
		themeTags.put("dark", "dark"); // Dark #37393A
		themeTags.put("sandalwood", "sandalwood"); // Sandy brown #DBA159
		themeTags.put("earth", "earth"); // Darker browns #322214
		themeTags.put("meadow", "meadow"); // Light green #8DE969
		themeTags.put("forest", "forest"); // Dark Green #1C5253
		themeTags.put("sky", "sky"); // Light blue #3581B8
		themeTags.put("ocean", "ocean"); // Dark blue #0D2149
		themeTags.put("cupcake", "cupcake"); // Light red/Pink #E0607E
		themeTags.put("scarlet", "scarlet"); // Dark reds #322214
		return themeTags;
	}
}
