package services.administration;

import com.tradr.springboot.view.storeclasses.PublicStoreDetails;
import com.tradr.springboot.view.storeclasses.StoreResponse;
import java.sql.*;
import java.util.concurrent.CompletableFuture;
import org.springframework.stereotype.Service;
import services.utils.AdminEnums;
import services.utils.DatabaseVerification;
import services.utils.TimeUtils;

@Service
public class AdministrationService {

	public boolean userIsAdmin(String authKey) {
		CompletableFuture<Boolean> userIsAdminCompletableFuture = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"SELECT * FROM users WHERE authKey = ?;"
					);
				) {
					statement.setString(1, authKey);
					ResultSet rs = statement.executeQuery();
					boolean isAdmin = false;
					if (!rs.next()) {
						return isAdmin;
					}
					do {
						isAdmin = rs.getBoolean("isAdmin");
					} while (rs.next());
					return isAdmin;
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);

		return userIsAdminCompletableFuture.join();
	}

	public AdminEnums moderateStore(
		byte moderationType,
		String moderationReason,
		PublicStoreDetails storeToModerate
	) {
		// 0 = Suspend, 1 = Disable
		if (moderationType == 0) {
			int existingSuspensionCount = storeToModerate.getSuspensionCount();
			int newSuspensionCount = existingSuspensionCount + 1;
			String suspensionExpiryTime = TimeUtils.getSuspensionDuration(
				existingSuspensionCount
			);

			CompletableFuture<Boolean> moderateStoreForSuspensionCompletableFuture = CompletableFuture.supplyAsync(() -> {
					try (
						Connection conn = DatabaseVerification.getConnection();
						PreparedStatement statement = conn.prepareStatement(
							"UPDATE stores SET isSuspended = ?, isSuspendedDuration = ?, isSuspendedReason = ?, suspensionCount = ? WHERE publicStoreId = ?"
						);
					) {
						statement.setBoolean(1, true);
						statement.setString(2, suspensionExpiryTime);
						statement.setString(3, moderationReason);
						statement.setInt(4, newSuspensionCount);
						statement.setString(
							5,
							storeToModerate.getPublicStoreId()
						);

						int rowsUpdated = statement.executeUpdate();
						return rowsUpdated > 0;
					} catch (SQLException e) {
						throw new RuntimeException(e);
					}
				}
			);

			if (moderateStoreForSuspensionCompletableFuture.join()) {
				return AdminEnums.STORE_SUSPENSION_SUCCESSFUL;
			}
			return AdminEnums.MODERATION_ACTION_FAILED;
		}

		if (moderationType == 1) {
			CompletableFuture<Boolean> moderateStoreForDisableCompletableFuture = CompletableFuture.supplyAsync(() -> {
					try (
						Connection conn = DatabaseVerification.getConnection();
						PreparedStatement statement = conn.prepareStatement(
							"UPDATE stores SET isDisabled = ?, isDisabledReason = ? WHERE publicStoreId = ?"
						);
					) {
						statement.setBoolean(1, true);
						statement.setString(2, moderationReason);
						statement.setString(
							3,
							storeToModerate.getPublicStoreId()
						);

						int rowsUpdated = statement.executeUpdate();
						return rowsUpdated > 0;
					} catch (SQLException e) {
						throw new RuntimeException(e);
					}
				}
			);

			if (moderateStoreForDisableCompletableFuture.join()) {
				return AdminEnums.STORE_DISABLED_SUCCESSFUL;
			}
			return AdminEnums.MODERATION_ACTION_FAILED;
		}

		return AdminEnums.MODERATION_ACTION_FAILED;
	}

	public AdminEnums reportStore(
		StoreResponse reportedStore,
		String reportingUUID
	) {
		CompletableFuture<Boolean> userHasAlreadyReportedThisStoreCompletableFuture = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"SELECT * FROM stores WHERE publicStoreId = ?"
					);
				) {
					statement.setString(
						1,
						reportedStore.getStore().getPublicStoreId()
					);

					ResultSet rs = statement.executeQuery();

					if (!rs.next()) {
						return false;
					}

					do {
						String[] reportedByUsers = rs
							.getString("reportedByUsers")
							.split(",");
						if (reportedByUsers.length > 0) {
							for (String individualUserUUID : reportedByUsers) {
								if (individualUserUUID.equals(reportingUUID)) {
									return true;
								}
							}
							return false;
						}
						return false;
					} while (rs.next());
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);

		if (userHasAlreadyReportedThisStoreCompletableFuture.join()) {
			return AdminEnums.USER_ALREADY_REPORTED;
		}

		CompletableFuture<Boolean> addReportToExistingStoreCompletableFuture = CompletableFuture.supplyAsync(() -> {
				try (
					Connection conn = DatabaseVerification.getConnection();
					PreparedStatement statement = conn.prepareStatement(
						"UPDATE stores SET reportedByUsers = ? WHERE publicStoreId = ?"
					);
				) {
					reportedStore
						.getStore()
						.getReportedByUsers()
						.add(reportingUUID);

					String reportingUsersAsString = String.join(
						",",
						reportedStore.getStore().getReportedByUsers()
					);

					statement.setString(1, reportingUsersAsString);
					statement.setString(
						2,
						reportedStore.getStore().getPublicStoreId()
					);

					int rowsUpdate = statement.executeUpdate();

					if (rowsUpdate > 0) {
						return true;
					}

					return false;
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
		);

		return addReportToExistingStoreCompletableFuture.join()
			? AdminEnums.USER_REPORT_SUCCESSFUL
			: AdminEnums.USER_REPORT_UNSUCCESSFUL;
	}
}
