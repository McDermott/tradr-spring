package com.tradr.springboot.view;

import java.sql.SQLException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import services.utils.DatabaseVerification;

@SpringBootApplication
@EnableScheduling
public class Application {

	public static void main(String[] args) throws SQLException {
		SpringApplication.run(Application.class, args);

		DatabaseVerification
			.databaseVerification()
			.validateUserTable()
			.validateStoreTable()
			.validateStoreItems()
			.validateStoreReviews();
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry
					.addMapping("/**")
					.allowedOrigins(
						"http://localhost:4200",
						"http://localhost:6006"
					);
			}
		};
	}
}
