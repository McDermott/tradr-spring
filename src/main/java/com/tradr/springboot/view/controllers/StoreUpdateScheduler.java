package com.tradr.springboot.view.controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import services.utils.DatabaseVerification;
import services.utils.LoggingUtils;

@Component
public class StoreUpdateScheduler {

	// @Scheduled(cron = "0 30 13 * * *", zone = "GMT") // Runs at 2:30 PM GMT
	@Scheduled(cron = "0 0 0 * * *", zone = "UTC") // Runs at 00:00 AM UTC
	public void updateStoresTable() {
		LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
			"yyyy-MM-dd'T'HH:mm:ss"
		);
		String currentDateTimeString = now.format(formatter);

		try {
			Connection conn = DatabaseVerification.getConnection();
			PreparedStatement statement = conn.prepareStatement(
				"UPDATE stores SET isSuspended = 0, isSuspendedDuration = '', isSuspendedReason = '', reportedByUsers = '' WHERE STR_TO_DATE(isSuspendedDuration, '%Y-%m-%dT%H:%i:%s') < STR_TO_DATE(?, '%Y-%m-%dT%H:%i:%s')"
			);

			statement.setString(1, currentDateTimeString);

			int update = statement.executeUpdate();
			if (update > 0) {
				LoggingUtils.log(
					update + " Store(s) released from suspension."
				);
			} else {
				LoggingUtils.log("No stores released from suspension.");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
