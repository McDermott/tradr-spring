package com.tradr.springboot.view.controllers;

import com.tradr.springboot.view.storeclasses.*;
import com.tradr.springboot.view.userclasses.UserAuthKey;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import services.registration.UserManagementService;
import services.storemanagement.StoreManagementService;
import services.utils.Environment;
import services.utils.StoreEnums;

@Controller
public class StoreController {

	private final StoreManagementService storeManagementService;
	private final UserManagementService userManagementService;

	public StoreController(
		StoreManagementService storeManagementService,
		UserManagementService userManagementService
	) {
		this.storeManagementService = storeManagementService;
		this.userManagementService = userManagementService;
	}

	@PostMapping("api/create-store")
	public ResponseEntity<StoreResponse> insertStore(
		@RequestBody Store storeToBeInserted
	) {
		StoreResponse storeResponse = new StoreResponse();

		StoreEnums resultFromService = storeManagementService.insertStore(
			storeToBeInserted,
			userManagementService
		);

		storeResponse.setStoreQueryResponseStatus(resultFromService);

		if (resultFromService != StoreEnums.STORE_INSERTED) {
			storeResponse.setStoreQueryResponseStatus(resultFromService);
			return ResponseEntity.badRequest().body(storeResponse);
		}

		storeResponse =
			storeManagementService.getIndividualStore(
				storeToBeInserted.getParentUUID(),
				false,
				userManagementService
			);

		return ResponseEntity.ok().body(storeResponse);
	}

	@PostMapping("api/update-store")
	public ResponseEntity<StoreEnums> updateStore(
		@RequestBody StoreUpdate storeToBeUpdated
	) {
		StoreEnums storeUpdatedResponse = storeManagementService.updateStore(
			storeToBeUpdated,
			userManagementService
		);
		return ResponseEntity.ok().body(storeUpdatedResponse);
	}

	@GetMapping("api/get-stores-list")
	public ResponseEntity<StoreSummaryResponse> getStoresListSummary(
		@RequestParam(value = "limit", required = false) String limit
	) {
		if (limit == null) {
			limit = Environment.DEFAULT_SEARCH_LIMIT;
		}
		StoreSummaryResponse storeSummaryResponse = storeManagementService.getStoresListSummaryFromDatabase(
			"",
			userManagementService,
			false,
			limit
		);
		return ResponseEntity.ok(storeSummaryResponse);
	}

	@GetMapping("api/get-store/{storeId}")
	public ResponseEntity<StoreResponse> getIndividualStore(
		@PathVariable("storeId") String storeId
	) {
		StoreResponse storeResponse = storeManagementService.getIndividualStore(
			storeId,
			false,
			userManagementService
		);
		return ResponseEntity.ok(storeResponse);
	}

	@PostMapping("api/insert-item")
	public ResponseEntity<StoreEnums> insertItemIntoExistingStore(
		@RequestBody StoreItemInsert itemToBeInserted
	) {
		UserAuthKey authKey = new UserAuthKey();
		authKey.setAuthKey(itemToBeInserted.getAuthKey());

		if (!userManagementService.isAuthKeyValid(authKey)) {
			return ResponseEntity
				.badRequest()
				.body(StoreEnums.ITEM_INSERTION_FAILED);
		}

		StoreEnums itemInsertionState = storeManagementService.prepareStoreItemsForInsertion(
			itemToBeInserted.getStoreItem(),
			itemToBeInserted.getParentUUID()
		);

		if (itemInsertionState == StoreEnums.ITEM_INSERTED) {
			return ResponseEntity
				.status(HttpStatus.OK)
				.body(StoreEnums.ITEM_INSERTED);
		}

		if (itemInsertionState == StoreEnums.ITEM_INSERTION_FAILED_PROFANITY) {
			return ResponseEntity
				.status(HttpStatus.OK)
				.body(StoreEnums.ITEM_INSERTION_FAILED_PROFANITY);
		}

		return ResponseEntity
			.status(HttpStatus.BAD_REQUEST)
			.body(StoreEnums.ITEM_INSERTION_FAILED);
	}

	@PostMapping("api/delete-item")
	public ResponseEntity<StoreEnums> deleteItemFromStore(
		@RequestBody StoreItemDeletion itemToBeDeleted
	) {
		UserAuthKey authKey = new UserAuthKey();
		authKey.setAuthKey(itemToBeDeleted.getAuthKey());

		if (!userManagementService.isAuthKeyValid(authKey)) {
			return ResponseEntity
				.badRequest()
				.body(StoreEnums.ITEM_DELETION_FAILED);
		}

		StoreEnums itemInsertionState = storeManagementService.deleteItem(
			itemToBeDeleted.getStoreItemPublicId(),
			itemToBeDeleted.getAuthKey(),
			userManagementService
		);

		if (itemInsertionState == StoreEnums.ITEM_DELETED) {
			return ResponseEntity
				.status(HttpStatus.OK)
				.body(StoreEnums.ITEM_DELETED);
		}

		return ResponseEntity
			.status(HttpStatus.BAD_REQUEST)
			.body(StoreEnums.ITEM_DELETION_FAILED);
	}

	@PostMapping("api/delete-store")
	public ResponseEntity<StoreEnums> deleteStore(
		@RequestBody UserAuthKey authKey
	) {
		if (!userManagementService.isAuthKeyValid(authKey)) {
			return ResponseEntity
				.badRequest()
				.body(StoreEnums.STORE_DELETION_FAILED);
		}

		String userOwnedStoreUUID = userManagementService.getUserOwnedStoreUUID(
			authKey.getAuthKey()
		);
		StoreEnums itemInsertionState = storeManagementService.deleteStore(
			authKey,
			userOwnedStoreUUID
		);

		String userUUID = storeManagementService.getUserUUID(
			authKey.getAuthKey()
		);

		boolean purgeUserOwnedStoreUUID = userManagementService.updateUserOwnedStoreUUID(
			userOwnedStoreUUID,
			userUUID,
			true
		);

		if (
			itemInsertionState == StoreEnums.STORE_DELETION_SUCCESSFUL &&
			purgeUserOwnedStoreUUID
		) {
			return ResponseEntity
				.status(HttpStatus.OK)
				.body(StoreEnums.STORE_DELETION_SUCCESSFUL);
		}

		return ResponseEntity
			.status(HttpStatus.BAD_REQUEST)
			.body(StoreEnums.STORE_DELETION_FAILED);
	}
}
