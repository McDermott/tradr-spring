package com.tradr.springboot.view.userclasses;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserDetailsForAssociatedStore {

	private String userName;
	private String firstName;
	private String lastName;
	// Moderation
	private boolean isSuspended;
	private boolean isDisabled;
}
