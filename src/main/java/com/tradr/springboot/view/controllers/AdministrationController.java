package com.tradr.springboot.view.controllers;

import com.tradr.springboot.view.adminclasses.ModerateStore;
import com.tradr.springboot.view.adminclasses.ReportStore;
import com.tradr.springboot.view.storeclasses.PublicStoreDetails;
import com.tradr.springboot.view.storeclasses.StoreResponse;
import com.tradr.springboot.view.storeclasses.StoreSummaryResponse;
import com.tradr.springboot.view.userclasses.UserAuthKey;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import services.administration.AdministrationService;
import services.registration.UserManagementService;
import services.storemanagement.StoreManagementService;
import services.utils.AdminEnums;
import services.utils.Environment;
// import services.utils.MockContentGenerator;
import services.utils.StoreEnums;

@Controller
public class AdministrationController {

	private AdministrationService administrationService;
	private StoreManagementService storeManagementService;
	private UserManagementService userManagementService;

	public AdministrationController(
		AdministrationService administrationService,
		StoreManagementService storeManagementService,
		UserManagementService userManagementService
	) {
		this.administrationService = administrationService;
		this.storeManagementService = storeManagementService;
		this.userManagementService = userManagementService;
	}

	@PostMapping("api/moderate-store")
	public ResponseEntity<AdminEnums> moderateStore(
		@RequestBody ModerateStore moderateStore
	) {
		UserAuthKey userAuthKey = new UserAuthKey();
		userAuthKey.setAuthKey(moderateStore.getAuthKey());

		if (!userManagementService.isAuthKeyValid(userAuthKey)) {
			return ResponseEntity
				.badRequest()
				.body(AdminEnums.ADMIN_NOT_AUTHORISED);
		}

		if (!administrationService.userIsAdmin(moderateStore.getAuthKey())) {
			return ResponseEntity
				.badRequest()
				.body(AdminEnums.ADMIN_NOT_AUTHORISED);
		}

		PublicStoreDetails storeToModerate = storeManagementService
			.getIndividualStore(
				moderateStore.getStorePublicId(),
				false,
				userManagementService
			)
			.getStore();

		AdminEnums moderationServiceResult = administrationService.moderateStore(
			moderateStore.getModerationType(),
			moderateStore.getModerationReason(),
			storeToModerate
		);

		return ResponseEntity.ok().body(moderationServiceResult);
	}

	@PostMapping("api/report-store")
	public ResponseEntity<AdminEnums> reportStore(
		@RequestBody ReportStore reportedStore
	) {
		UserAuthKey userAuthKey = new UserAuthKey();
		userAuthKey.setAuthKey(reportedStore.getAuthKey());
		if (!userManagementService.isAuthKeyValid(userAuthKey)) {
			return ResponseEntity
				.badRequest()
				.body(AdminEnums.USER_NOT_AUTHORISED);
		}

		StoreResponse storeResponse = storeManagementService.getIndividualStore(
			reportedStore.getStorePublicId(),
			true,
			userManagementService
		);

		if (
			storeResponse.getStoreQueryResponseStatus() ==
			StoreEnums.STORE_EMPTY
		) {
			return ResponseEntity
				.badRequest()
				.body(AdminEnums.USER_REPORT_UNSUCCESSFUL);
		}

		AdminEnums userReportStatus = administrationService.reportStore(
			storeResponse,
			reportedStore.getReportingUUID()
		);

		if (userReportStatus == AdminEnums.USER_ALREADY_REPORTED) {
			return ResponseEntity
				.badRequest()
				.body(AdminEnums.USER_ALREADY_REPORTED);
		}

		if (userReportStatus == AdminEnums.USER_REPORT_UNSUCCESSFUL) {
			return ResponseEntity
				.badRequest()
				.body(AdminEnums.USER_REPORT_UNSUCCESSFUL);
		}

		return ResponseEntity.ok().body(userReportStatus);
	}

	@PostMapping("api/get-reported-stores")
	public ResponseEntity<StoreSummaryResponse> getReportedStores(
		@RequestBody UserAuthKey authKey,
		@RequestParam(value = "limit", required = false) String limit
	) {
		if (limit == null) {
			limit = Environment.DEFAULT_SEARCH_LIMIT;
		}
		StoreSummaryResponse storeSummaryResponse = new StoreSummaryResponse();
		storeSummaryResponse.setStoreSummaryQueryStatus(
			StoreEnums.STORE_LIST_EMPTY
		);

		if (!userManagementService.isAuthKeyValid(authKey)) {
			return ResponseEntity.badRequest().body(storeSummaryResponse);
		}

		if (administrationService.userIsAdmin(authKey.getAuthKey())) {
			storeSummaryResponse =
				storeManagementService.getStoresListSummaryFromDatabase(
					"",
					userManagementService,
					true,
					limit
				);
			return ResponseEntity.ok().body(storeSummaryResponse);
		}

		return ResponseEntity.badRequest().body(storeSummaryResponse);
	}
	// @PostMapping("api/populate-mocks")
	// public ResponseEntity<String> populateMocks()
	// 	throws NoSuchAlgorithmException, SQLException {
	// 	MockContentGenerator.createUsersAndStores();

	// 	return ResponseEntity.ok().body("'ok'");
	// }
}
