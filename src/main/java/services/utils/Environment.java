package services.utils;

public class Environment {

	public static final String DB_URL = "jdbc:mysql://localhost/tutorialspoint";
	public static final String USER = "admin";
	public static final String PASS = "admin";
	public static final String EXTERNAL_BLOB_URL =
		"http://localhost:8080/blobs";
	public static final String DEFAULT_SEARCH_LIMIT = "20";
}
