package com.tradr.springboot.view.storeclasses;

import com.tradr.springboot.view.userclasses.UserDetailsForAssociatedStore;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class StoreSummary {

	private String storeTitle;
	private String storeDescription;
	private String storeTheme;
	private String ownUUID;
	private String publicStoreId;
	private int suspensionCount;
	private int reports;
	private UserDetailsForAssociatedStore associatedUser;
}
