package com.tradr.springboot.view.userclasses;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PublicUserDetails {

	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	private String dob;
	private String registrationDate;
	private String authKey;
	private String uuid;
	// Moderation
	private boolean isSuspended;
	private String isSuspendedDuration;
	private String isSuspendedReason;
	private int suspensionCount;
	private boolean isDisabled;
	private String isDisabledReason;
	private boolean isAdmin;
}
