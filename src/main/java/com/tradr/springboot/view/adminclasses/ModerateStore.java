package com.tradr.springboot.view.adminclasses;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ModerateStore {

	private String authKey;
	private String storePublicId;
	// 0 = Suspend, 1 = Disable
	private byte moderationType;
	private String moderationReason;
}
