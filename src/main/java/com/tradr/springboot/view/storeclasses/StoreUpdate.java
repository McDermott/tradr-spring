package com.tradr.springboot.view.storeclasses;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class StoreUpdate {

	private String authKey;
	private String storeDescription;
	private String storeTheme;
	private boolean isPrivate;
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;
	private String storePhone;
	private String postcode;
	private String storeAvatar;
	private String storeBanner;
}
